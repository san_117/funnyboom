﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class Tracker : DefaultTrackableEventHandler{

    public GameEvent OnTrackStart;
    public GameEvent OnTrackLost;

    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();
        OnTrackStart.Raise();
    }

    protected override void OnTrackingLost()
    {
        base.OnTrackingLost();
        OnTrackLost.Raise();
    }
}
